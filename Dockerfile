FROM alpine:latest

ENV HELM_VERSION=v2.5.1
ENV HELM_FILENAME=helm-${HELM_VERSION}-linux-amd64.tar.gz
ENV GCLOUD_SDK_VERSION="166.0.0"
ENV GCLOUD_SDK_URL="https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_SDK_VERSION}-linux-x86_64.tar.gz"
ENV PATH $PATH:/opt/google-cloud-sdk/bin

WORKDIR /

ADD ${GCLOUD_SDK_URL} /opt

RUN apk add --update ca-certificates bash openssh git gettext tar gzip scrub \
 && apk add --update -t deps curl  \
 && apk add --no-cache python \
 && curl -L https://storage.googleapis.com/kubernetes-helm/${HELM_FILENAME} | tar xz && mv linux-amd64/helm /bin/helm && chmod +x /bin/helm && rm -rf linux-amd64 \
 && chmod +x /opt/google-cloud-sdk/install.sh && ./opt/google-cloud-sdk/install.sh --command-completion=true --additional-components kubectl \
 && apk del --purge deps \
 && rm /var/cache/apk/*